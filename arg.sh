#!/bin/bash
file=$1
if [ $# -eq 0 ]
then
read -p "provide filename" file
fi
if [ $# -gt 1 ] 
then 
	echo "Only one argument expected, if filename has spaces in it use double quotes in argument"
	exit 0
fi
a=$(pwd)
if [ -f $a/$file ]
then 
	echo "file already exists"
	exit 0
else
 	date > $a/$file
fi



