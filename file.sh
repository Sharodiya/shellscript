1 #!/bin/bash
  2 count=1
  3 while [ $count -le 3 ]
  4 do
  5
  6         read -p "Enter the username:" user
  7         read -p "Enter password:" -s pass
  8         echo
  9         if [ ! -f $user.acc ]
 10         then
 11                 echo "User not registered. Please signup first"
 12                 exit 1
 13         fi
 14         md5=`echo $pass | md5sum | cut -f 1 -d " "`
 15         c=`cat $user.acc`
 16         if [ $md5 == $c ]
 17         then
 18                 echo "Login successful"
 19                 cat $user.profile
 20                 exit 0
 21         else
 22                 echo "Username or password incorrect"
 23                 if [ $count -eq 3 ]
 24                 then
 25                         echo "You have exceeded 3 login attempts"
 26                         exit 3
 27                 fi
 28                 read -p "Do you want to try again? Y/N" res
 29                 if [ "$res" == "Y" -o "$res" == "y" ]
 30                 then
 31                         count=`expr $count + 1`
 32                 elif [ "$res" == "N" -o "$res" == "n" ]
 33                 then
 34                         exit 0
 35                 else
 36                         echo "Aap andhe hai!!"
 37                         exit 2
 38                 fi
 39         fi
 40
 41 done
