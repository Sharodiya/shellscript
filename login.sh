#!/bin/bash
echo -e "0. Exit \n 1. Signup \n 2. Login"
read -p "Please choose option (0-2):" op
case $op in
	0)
		exit 1
		;;
	1)
		bash account.sh
		;;
	2)
		echo "Login page opened"
		;;
	*)
		echo "ERROR: Invalid option"
		exit 1
		;;
esac
